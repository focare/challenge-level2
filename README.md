# Desafio Focare 2 #


Desenvolver uma consulta no google retornando o titulo dos dois primeiros resultados.

### Entrada ###

* Valor a pesquisar;

### Etapas ###

* Abrir o google;
* Colocar o texto para pesquisar;
* Pesquisar;
* Ler informações;


Exemplo:

![Consulta](Consulta.png)

### Saída ###

Resultado dos dois primeiros titulos. Seguindo imagem do exemplo acima.

Ex: 

* Speedteste by Ookla – Teste de Velocidade de Conexão da ...

* Fast.com: Teste de velocidade da internet

### Requisitos técnicos ###

O desafio deve ser feito em linguagem de livre escolha ao desenvolvedor.

C# ou VB são diferenciais porem não determinantes.

### Entrega ###

A entrega deve ser feita por e-mail aos endereços abaixo com o código do desafio.

- daniel@focare.net.br
- victor@focare.net.br